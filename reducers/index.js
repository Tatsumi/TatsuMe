// Redux
import { combineReducers } from 'redux';

// Reducers
import { leaderboardFetched, leveledUp, gameSaved } from './userStatistics';

const appReducer = combineReducers({
	leaderboardFetched,
	leveledUp,
	gameSaved
});

export default rootReducer = (state, action) => {
	return appReducer(state, action);
}
