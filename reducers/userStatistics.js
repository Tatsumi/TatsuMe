export function leaderboardFetched(state = {}, action) {

	switch(action.type) {
		case 'LEADERBOARD_FETCHED':
			return { ...state, leaderBoard: action.payload };
		case 'LEADERBOARD_REJECTED':
			return { ...state, leaderBoard: action.error };
		default:
			return state;
	}

	return state;
}

export function gameSaved(state = {}, action) {

	switch(action.type) {
		case 'TO_LEADERBOARD_DONE':
			return { ...state, userData: action.payload };
		case 'TO_LEADERBOARD_REJECTED':
			return { ...state, userData: action.error };
		default:
			return state;
	}

	return state;
}

export function leveledUp(state = {}, action) {

	switch(action.type) {
		case 'LEVELUP_DONE':
			return { ...state, userLevel: action.payload };
		case 'LEVELUP_FAILED':
			return { ...state, userLevel: action.error };
		default:
			return state;
	}

	return state;
}
