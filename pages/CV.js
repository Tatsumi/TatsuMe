/**
 * @author Tatsumi Suzuki
 * @description CV component.
 */

// React
import React from 'react';

// React Native
import {
	AppRegistry,
	StyleSheet,
	ScrollView,
	Text,
	View,
	Linking,
	BackHandler,
	Platform
} from 'react-native';

// Redux
import { connect } from 'react-redux';

// Components
import TopNav from '../containers/navigation/TopNav';
import PageHero from '../containers/PageHero';
import PageContent from '../containers/PageContent';

import imgMe from '../assets/img/tatsumi-1.jpg';

// Styles
import applicationStyles from '../assets/applicationStyles';

const styles = StyleSheet.create({
	main: {
		width: '100%',
		height: '100%',
		flexDirection: 'column',
		alignContent: 'flex-start',
		backgroundColor: `${applicationStyles.backgroundColor}`
	},
	navBar: {
		width: '100%',
		height: 'auto',
		backgroundColor: `${applicationStyles.navBarBlue}`,
		justifyContent: 'flex-end',
		flexDirection: 'row'
	},
	textWrapper: {
		marginTop: -1,
		padding: 25,
		flexDirection: 'column',
		alignContent: 'center',
		backgroundColor: `${applicationStyles.overlayColor}`
	},
	text: {
		textAlign: 'center',
		margin: 5,
		color: `${applicationStyles.textColor}`
	},
	link: {
		textDecorationLine: 'underline',
	}
});

class CV extends React.Component {

	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {

		return (
			<ScrollView style={styles.main}>
				<View style={styles.navBar}>
					<TopNav {...this.props} />
				</View>
				<PageHero
					pageTitle="CV/CONTACT"
					pageHero={ imgMe }
				/>
				<View style={ styles.textWrapper }>
					<Text style={ styles.text }>
						You made it this far. Congratulations! Download my CV &nbsp;
						<Text style={ styles.link } onPress={ () => Linking.openURL('https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/tatsumi_suzuki_cv.pdf?alt=media&token=002c152f-e778-4d95-9951-7ec536163870') }>
							here
						</Text>
						<Text>
							&nbsp; or you can get in touch with me by sending me an e-mail to &nbsp;
						</Text>
						<Text style={ styles.link } onPress={ () => Linking.openURL('mailto:djtatsumisuzuki@gmail.com') }>
							djtatsumisuzuki@gmail.com.
						</Text>
					</Text>
				</View>
			</ScrollView>
		)

	}
}

function mapStateToProps(state) {

	return {
		leveledUp: state.leveledUp
	};
}

export default connect(mapStateToProps)(CV);

AppRegistry.registerComponent('tatsume', () => CV);
