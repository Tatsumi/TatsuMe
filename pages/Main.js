/**
 * @author Tatsumi Suzuki
 * @description Startpage for app.
 */

// React
import React from 'react';

// React Native
import {
	AppRegistry,
	Text,
	View,
	ScrollView,
	StyleSheet,
	Image,
	Dimensions
} from 'react-native';

// Redux
import { connect } from 'react-redux';

// Components
import TopNav from '../containers/navigation/TopNav';
import { TextHeader } from '../components/TextHeader';
import { TextParagraphWhite } from '../components/TextParagraphWhite';
import { AppButton } from '../components/AppButton';

// Other
import FontAwesome, { Icons } from 'react-native-fontawesome';

import imgMe from '../assets/img/tatsumi-1.jpg';

// Styles
import applicationStyles from '../assets/applicationStyles';

const styles = StyleSheet.create({
	main: {
		width: '100%',
		height: '100%',
		flexDirection: 'column',
		alignContent: 'flex-start',
		backgroundColor: `${applicationStyles.backgroundColor}`
	},
	navBar: {
		width: '100%',
		height: 'auto',
		backgroundColor: `${applicationStyles.navBarBlue}`,
		flexDirection: 'row',
		justifyContent: 'flex-end'
	},
	heroWrapper: {
		overflow: 'hidden'
	},
	hero: {
		width: Dimensions.get('window').width,
		height: Dimensions.get('window').width < 768 ? 251 : 683
	},
	textWrapper: {
		width: '100%',
		height: '100%',
		flexDirection: 'column',
		alignContent: 'flex-start',
		justifyContent: 'flex-start',
		backgroundColor: `${applicationStyles.overlayColor}`,
		paddingLeft: 15,
		paddingRight: 15
	},
	h1: {
		color: 'white'
	}
});

export class Main extends React.Component {

	constructor(props) {
		super(props);

		this.state = {};
	}

	_onPress(event) {
		if(typeof event.buttonPressed !== 'undefined' && event.buttonPressed === true) {
			this.props.navigation.navigate('GamePage')
		}
	}

	shouldComponentUpdate() {
		return true;
	}

	render() {

		return (
			<ScrollView style={styles.main}>
				<View style={ styles.navBar }>
					<TopNav { ...this.props } />
				</View>
				<View style={styles.heroWrapper}>
					<Image
						style={styles.hero}
						resizeMode="cover"
						source={ imgMe }
					/>
				</View>
				<View style={styles.textWrapper}>
					<TextHeader margin={15}>
						<FontAwesome>{Icons.infoCircle}</FontAwesome>&nbsp;HELLO WORLD!
					</TextHeader>
					<TextParagraphWhite margin={10}>
						<Text style={{ fontWeight: 'bold' }}>This is me then</Text>. I’m Tatsumi Suzuki 38 years old from Stockholm.
						I like to call myself an entrepreneur, fullstack developer and an application designer.
					</TextParagraphWhite>
					<TextParagraphWhite margin={10}>
						I have developed a little game where you as my visitor will play to gain access to parts of this app with my work and eventually my CV with contact information.
					</TextParagraphWhite>
					<AppButton onPressEvent={(event) => this._onPress(event)}>
						START GAME
					</AppButton>
				</View>
			</ScrollView>

		)

	}
}

function mapStateToProps(state) {

	return {
		leaderboardFetched: state.leaderboardFetched,
		leveledUp: state.leveledUp
	};
}

export default connect(mapStateToProps)(Main);

AppRegistry.registerComponent('tatsume', () => Main);



