/**
 * @author Tatsumi Suzuki
 * @description Display work grid or specific work if image has been clicked.
 */

// React
import React from 'react';

// React Native
import {
	AppRegistry,
	StyleSheet,
	ScrollView,
	View,
	Image,
	TouchableOpacity,
	Dimensions,
	BackHandler
} from 'react-native';

// Redux
import { connect } from 'react-redux';

// Components
import TopNav from '../containers/navigation/TopNav';
import PageHero from '../containers/PageHero';
import PageContent from '../containers/PageContent';

// Content
import pageContentStrings from '../assets/pageContentStrings';

// Styles
import applicationStyles from '../assets/applicationStyles';
import {TextHeader} from "../components/TextHeader";

const windowWidth = Dimensions.get('window').width;
const imagesPerRow = 2;
const size = windowWidth / imagesPerRow - 30;

class Work extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			loadPage: null
		};

		this.handleBackPress = this.handleBackPress.bind(this);
	}

	static getDerivedStateFromProps(nextProps, prevState) {

		// If Work has been clicked in the drawer. We wan't to clear loadPage.
		// showGrid = true is sent from MainMenu.
		const showGrid = nextProps.navigation.state.params.showGrid;

		if(typeof showGrid !== 'undefined' && showGrid === true) {
			return {
				loadPage: null
			}
		}

		return null;
	}

	_calculatedSize() {

		return {
			width: size,
			height: size
		}
	}

	_showPageContent(pageContent) {

		this.setState({
			loadPage: pageContent
		});

	}

	componentDidMount() {
		BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
	}

	componentWillUnmount() {
		BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
	}

	handleBackPress() {

		if(this.state.loadPage === null) {
			this.props.navigation.navigate('Main');
		} else {
			this.props.navigation.navigate('Work', {
				showGrid: true
			});
		}
		BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress);
		return true;
	};

	render() {

		const styles = StyleSheet.create({
			main: {
				width: '100%',
				height: '100%',
				flexDirection: 'column',
				backgroundColor: `${applicationStyles.backgroundColor}`
			},
			navBar: {
				width: '100%',
				height: 'auto',
				backgroundColor: `${applicationStyles.navBarBlue}`,
				flexDirection: 'row',
				justifyContent: 'flex-end'
			},
			imagesWrapper: {
				flex:1,
				flexDirection: 'row',
				justifyContent: 'space-around',
				flexWrap: 'wrap',
			},
			image: {
				width: size,
				marginTop: 15,
			},
			text: {
				width: size,
				marginBottom: 15,
				padding: 5,
				backgroundColor: `${applicationStyles.overlayColor}`
			},
			heroWrapper: {
				overflow: 'hidden'
			},
			hero: {
				height: 251
			},
		});

		return (
			<ScrollView style={ styles.main }>
				{
					this.state.loadPage !== null ? (
						<View>
							<View style={ styles.navBar }>
								<TopNav { ...this.props } clearState={ this.state.loadPage !== null } inWorkPage={true}/>
							</View>
							<PageHero
								pageTitle={ this.state.loadPage.title }
								pageHero={ this.state.loadPage.image }
								tags={ this.state.loadPage.tags }
							/>
							<PageContent
								introText={ this.state.loadPage.introText }
								longText={ this.state.loadPage.longText }
								linkTo={ this.state.loadPage.linkTo }
								linkTitle={ this.state.loadPage.linkTitle }
							/>
						</View>
					) : (
						<View>
							<View style={ styles.navBar }>
								<TopNav { ...this.props } clearState={ this.state.loadPage !== null } inWorkPage={false}/>
							</View>
							<PageHero pageTitle="WORK"/>
							<View style={ styles.imagesWrapper }>
								{
									Object.keys(pageContentStrings).map((page) =>
										page !== 'aboutPage' &&
										page !== 'aboutMe' ?
										typeof pageContentStrings[page].imageSquare !== 'undefined' ? (
											<TouchableOpacity
												key={ 'page-' + page }
												activeOpacity={0.85}
												onPress={ () =>  this._showPageContent(pageContentStrings[page])}
											>
												<Image
													style={[styles.image, this._calculatedSize()]}
													resizeMode="contain"
													source={{uri: pageContentStrings[page].imageSquare }}
												/>
												{
													typeof pageContentStrings[page].title !== 'undefined' ? (
														<View style={ styles.text }>
															<TextHeader
																align="center"
																color={ applicationStyles.textColor }
															>
																{ pageContentStrings[page].title }
															</TextHeader>
														</View>
													) : null
												}
											</TouchableOpacity>
										) : null
										: null)
								}
							</View>
						</View>
					)
				}
			</ScrollView>
		)
	}
}

function mapStateToProps(state) {

	return {
		leveledUp: state.leveledUp
	};
}

export default connect(mapStateToProps)(Work);

AppRegistry.registerComponent('tatsume', () => Work);
