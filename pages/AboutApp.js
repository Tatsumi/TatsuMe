/**
 * @author Tatsumi Suzuki
 * @description About this app component.
 */

// React
import React from 'react';

// React Native
import {
	AppRegistry,
	StyleSheet,
	ScrollView,
	View
} from 'react-native';

// Redux
import {connect} from 'react-redux';

// Components
import TopNav from '../containers/navigation/TopNav';
import PageHero from '../containers/PageHero';
import PageContent from '../containers/PageContent';

// Content
import pageContentStrings from '../assets/pageContentStrings';

import imgMe from '../assets/img/tatsumi-1.jpg';

// Styles
import applicationStyles from '../assets/applicationStyles';

const styles = StyleSheet.create({
	main: {
		width: '100%',
		height: '100%',
		flexDirection: 'column',
		alignContent: 'flex-start',
		backgroundColor: `${applicationStyles.backgroundColor}`
	},
	navBar: {
		width: '100%',
		height: 'auto',
		backgroundColor: `${applicationStyles.navBarBlue}`,
		justifyContent: 'flex-end',
		flexDirection: 'row'
	},
});


class AboutApp extends React.Component {

	constructor(props) {
		super(props);

		this.state = {};

	}

	render() {

		return (
			<ScrollView style={styles.main}>
				<View style={styles.navBar}>
					<TopNav {...this.props} />
				</View>
				<PageHero
					pageTitle={ pageContentStrings.aboutPage.title }
					pageHero={ imgMe }
					tags={ pageContentStrings.aboutPage.tags }
				/>
				<PageContent
					introText={ pageContentStrings.aboutPage.introText }
					longText={ pageContentStrings.aboutPage.longText }
					linkTo={ pageContentStrings.aboutPage.linkTo }
					linkTitle={ pageContentStrings.aboutPage.linkTitle }
				/>
			</ScrollView>
		)

	}
}

function mapStateToProps(state) {

	return {
		leveledUp: state.leveledUp
	};
}

export default connect(mapStateToProps)(AboutApp);

AppRegistry.registerComponent('tatsume', () => AboutApp);



