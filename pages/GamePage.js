/**
 * @author Tatsumi Suzuki
 * @description The TatsuME game.
 */

// React
import React from 'react';

// React Native
import {
	AppRegistry,
	StyleSheet,
	Text,
	ScrollView,
	View,
	TouchableHighlight,
	TextInput
} from 'react-native';

// Redux
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

// Actions
import { levelUp, saveGame } from '../actions/userStatistics';

// Components
import TopNav from '../containers/navigation/TopNav';
import { StatisticsContainer } from '../containers/liveStatistics/StatisticsContainer';
import LeaderBoard from '../containers/liveStatistics/LeaderBoard';
import { TextHeader } from '../components/TextHeader';
import { AppButton } from '../components/AppButton';
import IconUser from '../components/gameComponents/iconUser';
import IconBug from '../components/gameComponents/iconBug';
import IconMergeConflict from '../components/gameComponents/iconMergeConflict';
import IconFunction from '../components/gameComponents/iconFunction';
import IconRelease from '../components/gameComponents/iconRelease';
import IconDeadline from '../components/gameComponents/iconDeadline';

import AnimatedIcon from '../components/gameComponents/animatedIcon';

import FontAwesome, { Icons } from 'react-native-fontawesome';

// Styles
import applicationStyles from '../assets/applicationStyles';

const styles = StyleSheet.create({
	main: {
		flex: 1,
		flexDirection: 'column',
		alignContent: 'space-around',
		backgroundColor: `${applicationStyles.backgroundColor}`
	},
	navBar: {
		width: '100%',
		height: 'auto',
		backgroundColor: `${applicationStyles.navBarBlue}`,
		justifyContent: 'flex-end',
		flexDirection: 'row'
	},
	statsWrapper: {
		flex: 1,
		marginTop: 50,
		marginRight: 30,
		marginBottom: 50,
		marginLeft: 30,
		padding: 15,
		borderRadius: 3,
		flexDirection: 'column',
		backgroundColor: `${applicationStyles.overlayColor}`
	},
	leaderBoard: {
		flex: 1,
		flexDirection: 'column',
		alignItems: 'center',
		marginTop: 15,
		marginBottom: 15,
	},
	gameCanvas: {
		flex: 2,
		margin: 15,
		borderRadius: 3,
		flexDirection: 'row',
		justifyContent: 'space-between',
		backgroundColor: `${applicationStyles.overlayColor}`
	},
	canvasColumn: {
		width: 26,
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'space-between',
		position: 'relative'
	},
	columnItem: {
		position: 'absolute',
		left: 0
	},
	userColumn: {
		height: 56,
		flexDirection: 'column',
	},
	textWrapper: {
		margin: 15
	},
	textRow: {
		justifyContent: 'flex-start',
		marginTop: 15,
		flexDirection: 'row',
		flexWrap: 'wrap'
	},
	boldText: {
		marginRight: 5,
		fontWeight: 'bold',
		color: `${applicationStyles.textColor}`
	},
	text: {
		color: `${applicationStyles.textColor}`
	},
	buttonWrapper: {
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'flex-end'
	},
	arrowWrapper: {
		flexDirection: 'row',
	},
	arrow: {
		width: '50%',
		height: 70,
		backgroundColor: `${applicationStyles.overlayColor}`,
		borderRadius: 3,
		alignItems: 'center',
		justifyContent: 'center',
		borderWidth: 1,
		borderColor: `${applicationStyles.backgroundColor}`,
		borderStyle: 'solid'
	},
	arrowIcon: {
		color: `${applicationStyles.textColor}`,
		fontSize: 30
	},
	textFieldWrapper: {
		margin: 30,
	},
	textField: {
		height: 40,
		color: `${applicationStyles.textColor}`,
		borderColor: `${applicationStyles.ultraViolet}`,
		borderWidth: 1,
		backgroundColor: `${applicationStyles.backgroundColor}`,
		borderRadius: 3
	}
});


export class GamePage extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			nickName: '',
			gameStartedState: 0, // 0 = Show toplist, 1 = Show rules, 2 = Game has started.
			level: 1, // Level 1-9. Levels 3, 6 and 9 will unlock link's in the drawer.
			totalTime: 0,
			totalScore: 0,
			score: {
				bugs: 0, // Remove 10 points.
				mergeConflicts: 0, // Remove 10 points.
				functions: 0, // Add 10 points.
				releases: 0, // Add 10 points.
				deadlines: 0, // Add 10 points.
				lifes: 3 // Remove lifes with 1 point for all misses, bugs or merge conflicts.
			},
			notification: [
				'LEVEL UP ACHIEVEMENTS',
				'ABOUT ME UNLOCKED',
				'WORK UNLOCKED',
				'ALL STAGES COMPLETED',
			],
			notificationsShown: 0,
			userPosition: 6,
			columns: [{},{},{},{},{},{},{},{},{},{},{}]
		};
	}

	/**
	 * Fill columns with random items
	 */
	componentDidMount() {
		this._updateRandomColumnItemNr();
		this._startScoreTimer();
	}

	_startScoreTimer() {
		let timer = setInterval(() => {

			if(this.state.score.lifes <= 0) {
				clearInterval(timer);
			} else {

				let currentTime = this.state.totalTime;
				this.setState({
					totalTime: parseInt(currentTime) + 1
				});
			}

		}, 1000)
	}

	/**
	 * Update state.columns with one random component in one of the 11 columns.
	 * @param {number|null} oldColumn
	 * @private
	 */
	_updateRandomColumnItemNr(oldColumn = null) {

		const nextComponent = Math.floor(Math.random() * 5);
		let columnPosition = Math.floor(Math.random() * 10);
		let updateColumns = this.state.columns;

		// We don't wan't to use the same column again.
		if(oldColumn === columnPosition) {

			if(columnPosition === 10) {
				columnPosition = 9;
			} else if(columnPosition === 0) {
				columnPosition = 1;
			} else {
				columnPosition = Math.floor(Math.random() * columnPosition)
			}
		}

		updateColumns[columnPosition] = {
			columnItem: nextComponent
		};

		this.setState({
			columns: updateColumns
		});
	}

	/**
	 * Move me sideways one position if possible.
	 * @param {string} direction 'left|right'
	 * @private
	 */
	_btnPressed(direction) {

		let currentPosition = this.state.userPosition;

		if(direction === 'left' && currentPosition > 1) {
			currentPosition = currentPosition - 1;
		} else if(direction === 'right' && currentPosition < Object.keys(this.state.columns).length) {
			currentPosition = currentPosition + 1;
		}

		this.setState({
			userPosition: currentPosition
		});
	}

	/**
	 * Icon has reached the bottom.
	 * Update scores and run update new column item for the state.colums.
	 * @param {number} column
	 * @private
	 */
	_animationDone(column) {

		// Game has started again. Run timer with 2 sec and clear levels.
		if(this.state.gameStartedState === 2 && this.state.totalTime === 0) {
			this._startScoreTimer();

			this.setState({
				totalScore: 0,
				level: 1
			});
		}

		const userPosition = this.state.userPosition;
		const icon = parseInt(this.state.columns[column].columnItem);
		let updateScore = this.state.score;

		// Clear this column from the old item.
		let updateCurrentState = this.state.columns;
		updateCurrentState[column] = {};

		// Add|Remove item count and add points when needed.
		let skipPoint = false;

		// User took the icon.
		if(parseInt(column) === parseInt(userPosition) -1) {

			switch(icon) {
				case 0:
					// User took a bug. Remove a life and skip points.
					skipPoint = true;
					updateScore.bugs = parseInt(updateScore.bugs) + 1;
					updateScore.lifes = parseInt(updateScore.lifes) - 1;
					break;
				case 1:
					// User took a merge conflict. Remove a life and skip points.
					skipPoint = true;
					updateScore.mergeConflicts = parseInt(updateScore.mergeConflicts) + 1;
					updateScore.lifes = parseInt(updateScore.lifes) - 1;
					break;
				case 2:
					updateScore.functions = parseInt(updateScore.functions) + 1;
					break;
				case 3:
					updateScore.releases = parseInt(updateScore.releases) + 1;
					break;
				case 4:
					updateScore.deadlines = parseInt(updateScore.deadlines) + 1;
					break;
			}

		} else {

			switch(icon) {
				case 2:
				case 3:
				case 4:
					// User skipped good icons. Remove a life and skip points.
					skipPoint = true;
					updateScore.lifes = parseInt(updateScore.lifes) - 1;
					break;
				default:
					break;
			}

		}

		let totalScore = this.state.totalScore;
		let level  = this.state.level;

		if(! skipPoint) {

			let points = 3;

			// Check points and level up if needed.
			Object.keys(updateScore).map((i) => {

				if(i !== 'lifes') {
					points = points + parseInt(updateScore[i]);
				}
			});

			totalScore = (parseInt(points) * 2) * (parseInt(this.state.totalTime) / 2);

			if(totalScore < 100) {
				level = 1;
			}
			else if(totalScore > 100 && totalScore < 200) {
				level = 2;
			} else if(totalScore > 200 && totalScore < 300) {
				level = 3;
			} else if(totalScore > 300 && totalScore < 400) {
				level = 4;
			} else if(totalScore > 400 && totalScore < 500) {
				level = 5;
			} else if(totalScore > 500 && totalScore < 600) {
				level = 6;
			} else if(totalScore > 600 && totalScore < 700) {
				level = 7;
			} else if(totalScore > 700 && totalScore < 800) {
				level = 8;
			} else if(totalScore > 900) {
				level = 9;
			}
		}

		this.setState({
			level: level,
			totalScore: totalScore,
			score: updateScore,
			columns: updateCurrentState
		});

		this._updateRandomColumnItemNr(column);
	}

	_renderRandomIcon(column) {
		const columnData = this.state.columns[column];

		if(typeof columnData.columnItem !== 'undefined') {

			switch(columnData.columnItem) {

				case 0:
					return (
						<IconBug/>
					);
					break;
				case 1:
					return(
						<IconMergeConflict/>
					);
					break;
				case 2:
					return (
						<IconFunction/>
					);
					break;
				case 3:
					return(
						<IconRelease/>
					);
					break;
				case 4:
					return(
						<IconDeadline/>
					);
					break;
				default:
					break;
			}
		}

		return null;
	}

	_notificationShown(notification) {

		if(notification >= 1) {
			this.props.levelUp(notification);
		}

		this.setState({
			notificationsShown: notification
		});
	}

	/**
	 * Save game only if nickname is set.
	 * @private
	 */
	_saveGame() {

		if(this.state.nickName !== '') {
			this.props.saveGame(this.state.nickName, this.state.totalScore);
		}

		this.setState({
			gameStartedState: 0,
			score: {
				bugs: 0,
				mergeConflicts: 0,
				functions: 0,
				releases: 0,
				deadlines: 0,
				lifes: 3
			},
			notificationsShown: 1,
			totalTime: 0,
			userPosition: 6
		});
	}

	render() {

		let clearScore = this.state.score;

		if(clearScore.lifes <= 0) {
			clearScore = {
				bugs: 0,
				mergeConflicts: 0,
				functions: 0,
				releases: 0,
				deadlines: 0,
				lifes: 3
			};
		}

		return (
			<View style={styles.main}>
				<View style={styles.navBar}>
					{
						this.state.gameStartedState === 1 && this.state.notificationsShown === 0 ? (
							<View>
								<TopNav { ...this.props } notification={ this.state.notification[0] } shown={() => this._notificationShown(1)}/>
							</View>
						) : this.state.notificationsShown === 1 && parseInt(this.state.level) === 3 ? (
							<View>
								<TopNav { ...this.props } notification={ this.state.notification[1] } shown={() => this._notificationShown(2)} />
							</View>
						) : this.state.notificationsShown === 2 && parseInt(this.state.level) === 6 ? (
							<View>
								<TopNav { ...this.props } notification={ this.state.notification[2] } shown={() => this._notificationShown(3)}/>
							</View>
						) : this.state.notificationsShown === 3 && parseInt(this.state.level) === 9 ? (
							<View>
								<TopNav { ...this.props } notification={ this.state.notification[3] } shown={() => this._notificationShown(4)}/>
							</View>
						) : (
							<View>
								<TopNav { ...this.props } />
							</View>
						)
					}
				</View>
				{
					this.state.gameStartedState > 1  && this.state.score.lifes > 0 ? (
						<StatisticsContainer { ...this.state } />
					) : null
				}
				{
					this.state.gameStartedState === 0 ? (
						<ScrollView style={ styles.statsWrapper }>
							<TextHeader
								align="center"
								color={ applicationStyles.textColor }
							>
								LEADERBOARD
							</TextHeader>
							<View style={ styles.leaderBoard }>
								<LeaderBoard { ...this.props } />
							</View>
							<AppButton onPressEvent={() => this.setState({ gameStartedState: 1 })}>NEW GAME</AppButton>
						</ScrollView>
					) : this.state.gameStartedState === 1 ? (
						<ScrollView style={ styles.statsWrapper }>
							<TextHeader
								align="center"
								color={ applicationStyles.textColor }
							>
								ARE YOU READY?
							</TextHeader>
							<View style={ styles.textWrapper }>
								<View style={ styles.textRow }>
									<Text style={ styles.boldText }>Rules:</Text>
									<Text style={ styles.text }>You have 3 lifes. Use arrows to move from side to side. Unlock application parts on level 3, 6 and 9.</Text>
								</View>
								<View style={ styles.textRow }>
									<Text style={ styles.boldText }>Avoid bugs:</Text>
									<IconBug />
								</View>
								<View style={ styles.textRow }>
									<Text style={ styles.boldText }>Avoid merge conflicts:</Text>
									<IconMergeConflict />
								</View>
								<View style={ styles.textRow }>
									<Text style={ styles.boldText }>Catch functions:</Text>
									<IconFunction />
								</View>
								<View style={ styles.textRow }>
									<Text style={ styles.boldText }>Catch releases:</Text>
									<IconRelease />
								</View>
								<View style={ styles.textRow }>
									<Text style={ styles.boldText }>Catch deadlines:</Text>
									<IconDeadline />
								</View>
							</View>
							{
								this.state.notificationsShown > 0 ? (
									<View style={ styles.buttonWrapper }>
										<AppButton onPressEvent={() => this.setState({ gameStartedState: 2 })}>START</AppButton>
									</View>
								) : null
							}

						</ScrollView>
					) : this.state.gameStartedState === 2 && this.state.score.lifes > 0 ? (
						<View style={ styles.gameCanvas }>
							{
								Object.keys(this.state.columns).map((column) => (
									<View key={ 'canvas-column-' + column }>
										<View style={ styles.canvasColumn }>
											{
												typeof this.state.columns[column].columnItem !== 'undefined' ? (
													<AnimatedIcon
														animationDone={() => this._animationDone(column)}
													>
														{
															this._renderRandomIcon(column)
														}
													</AnimatedIcon>
												) : null
											}
										</View>
										<View style={ styles.userColumn }>
											{
												parseInt(column) + 1 === this.state.userPosition ? (
													<IconUser />
												) : null
											}
										</View>
									</View>
								))
							}
						</View>
					) : this.state.gameStartedState === 2 && this.state.score.lifes <= 0 ? (
						<ScrollView style={ styles.statsWrapper }>
							<TextHeader
								align="center"
								color={ applicationStyles.textColor }
							>
								GAME OVER
							</TextHeader>
							<View style={ styles.textRow }>
								<Text style={ styles.boldText }>ACHIEVEMENTS:</Text>
								{
									this.state.level === 3 ? (
										<Text style={ styles.text }>You have unlocked ABOUT ME. You can see the result in the main menu. To unlock all you need to get to level 9.</Text>
									) : this.state.level === 6 ? (
										<Text style={ styles.text }>You have unlocked ABOUT ME and WORK. You can see the result in the main menu. To unlock all you need to get to level 9.</Text>
									) : this.state.level === 9 ? (
										<Text style={ styles.text }>You have unlocked ABOUT ME, WORK and CV/CONTACT. You can see the result in the main menu.</Text>
									) : (
										<Text style={ styles.text }>No achievements unlocked. To unlock all you need to get to level 9. Better luck next time.</Text>
									)
								}
							</View>
							<View style={ styles.buttonWrapper }>
								<AppButton
									onPressEvent={() => this.setState({
										gameStartedState: 2,
										score: clearScore,
										notificationsShown: 1,
										totalTime: 0,
										userPosition: 6
									})}
								>
									PLAY AGAIN
								</AppButton>
								<AppButton onPressEvent={() => this.setState({ gameStartedState: 3 })}>NEXT</AppButton>
							</View>
						</ScrollView>
					) : (
						<ScrollView style={ styles.statsWrapper }>
							<TextHeader
								align="center"
								color={ applicationStyles.textColor }
							>
								SAVE YOUR POINTS
							</TextHeader>
							<View style={ styles.textRow }>
								<Text style={ styles.boldText }>Optional</Text>
								<Text style={ styles.text }>If you wish to save the game and be part of the leaderboard, enter your nickname below.</Text>
								<Text style={ styles.text }>Otherwise, leave it empty and click and continue.</Text>
							</View>
							<View style={styles.textFieldWrapper}>
								<TextInput
									style={styles.textField}
									onChangeText={(text) => this.setState({ nickName:text })}
									value={this.state.nickName}
								/>
							</View>
							<View style={ styles.buttonWrapper }>
								<AppButton onPressEvent={() => this._saveGame()}>
									CONTINUE
								</AppButton>
							</View>
						</ScrollView>
					)
				}
				{
					this.state.gameStartedState > 1 && this.state.score.lifes > 0? (
						<View style={ styles.arrowWrapper }>
							<TouchableHighlight
								style={ styles.arrow }
								underlayColor={ applicationStyles.overlayColor }
								onPress={() => this._btnPressed('left')}
							>
								<View>
									<FontAwesome color={ applicationStyles.textColor } style={ styles.arrowIcon }>{ Icons.chevronLeft }</FontAwesome>
								</View>
							</TouchableHighlight>
							<TouchableHighlight
								style={ styles.arrow }
								underlayColor={ applicationStyles.overlayColor }
								onPress={() => this._btnPressed('right')}
							>
								<View>
									<FontAwesome color={ applicationStyles.textColor } style={ styles.arrowIcon }>{ Icons.chevronRight }</FontAwesome>
								</View>
							</TouchableHighlight>
						</View>
					) : null
				}
			</View>
		)
	}
}

function mapStateToProps(state) {

	return {
		leaderboardFetched: state.leaderboardFetched,
		leveledUp: state.leveledUp
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		levelUp,
		saveGame
	}, dispatch);
}


export default connect(mapStateToProps, mapDispatchToProps)(GamePage);

AppRegistry.registerComponent('tatsume', () => GamePage);



