/**
 * @author Tatsumi Suzuki
 * @description Handle for all routes in the stack navigator
 */

// Components
import Mount from '../containers/Mount';
import Main from '../pages/Main';
import GamePage from '../pages/GamePage';
import AboutApp from '../pages/AboutApp';
import Work from '../pages/Work';
import AboutMe from '../pages/AboutMe';
import CV from '../pages/CV';

const StackNavigatorRoutes = {
	Mount: { screen: Mount },
	Main: { screen: Main },
	GamePage: { screen: GamePage },
	AboutApp: { screen: AboutApp },
	Work: { screen: Work },
	AboutMe: { screen: AboutMe },
	CV: { screen: CV }
};

export default StackNavigatorRoutes;
