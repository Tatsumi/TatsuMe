/**
 * @author Tatsumi Suzuki
 * @description User statistics
 */

import firebase from '../assets/database';
const database = firebase.database();

export function fetchLeaderboard() {

	return dispatch => {

		database.ref('leaderboard').on('value', snap => {

			if(snap.exists) {

				dispatch({
					type: 'LEADERBOARD_FETCHED',
					payload: snap.val()
				});

			} else {
				dispatch({
					type: 'LEADERBOARD_REJECTED',
					error: 'No leaderboard found'
				});
			}
		});
	}
}


export function saveGame(nickname = '', score = 0) {

	return dispatch => {

		if(nickname !== '' && score !== 0) {

			let dateObj = new Date(),
				month = dateObj.getUTCMonth() + 1,
				day = dateObj.getUTCDate(),
				year = dateObj.getUTCFullYear(),
				newDate = year + '-' + month + '-' + day;

			database.ref('leaderboard/' + score).set({
				nickname: nickname,
				date: newDate
			}).then(() => {
				dispatch({
					type: 'TO_LEADERBOARD_DONE',
					payload: {
						nickname: nickname,
						score: score
					}
				});
			});

		} else {
			dispatch({
				type: 'TO_LEADERBOARD_REJECTED',
				error: 'No nick or score found'
			});
		}
	}
}

export function levelUp(to = null) {

	return dispatch => {

		if(to === null) {
			dispatch({
				type: 'LEVELUP_FAILED',
				error: 'Error. No value to level up to.'
			});
		} else {
			dispatch({
				type: 'LEVELUP_DONE',
				payload: to
			});
		}
	}
}
