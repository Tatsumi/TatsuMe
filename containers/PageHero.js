/**
 * @author Tatsumi Suzuki
 * @description Page hero.
 */

// React
import React from 'react';

// React Native
import {
    View,
	Image,
	StyleSheet,
	AppRegistry,
	Dimensions
} from 'react-native';

// Components
import { TextHeader } from '../components/TextHeader';

// Other
import FontAwesome, { Icons } from 'react-native-fontawesome';

// Styles
import applicationStyles from '../assets/applicationStyles';


export default class PageHero extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		const styles = StyleSheet.create({
			heroWrapper: {
				width: '100%',
				height: 'auto',
				backgroundColor: `${applicationStyles.overlayColor}`
			},
			hero: {
				width: Dimensions.get('window').width,
				height: Dimensions.get('window').width < 768 ? 251 : 683
			},
			tagWrapper: {
				marginTop: 15,
				flexDirection: 'row',
				justifyContent: 'center',
				flexWrap: 'wrap',
			},
			tag: {
				margin: 15,
				paddingTop: 6,
				paddingRight: 10,
				paddingBottom: 6,
				paddingLeft: 10,
				backgroundColor: `${applicationStyles.textColor}`,
				borderRadius: 3,
				flexDirection: 'row',
				justifyContent: 'center'
			}
		});

		const img = typeof this.props.pageHero === 'string' ? {uri: this.props.pageHero } : this.props.pageHero;

		return (
			<View style={ styles.heroWrapper }>
				{
					typeof this.props.pageTitle !== 'undefined' ? (
						<TextHeader
							align="center"
							margin={10}
						>
							{ this.props.pageTitle }
						</TextHeader>
					) : null
				}
				{
					typeof this.props.pageHero !== 'undefined' ? (
						<Image
							style={styles.hero}
							resizeMode="cover"
							source={img}
						/>
					) : null
				}
				{
					typeof this.props.tags !== 'undefined' ? (
						<View style={ styles.tagWrapper }>
							{
								Object.keys(this.props.tags).map((tag) => (
									<View style={ styles.tag } key={ 'tag-' + tag }>
										<FontAwesome style={{alignSelf: 'center'}} color={ applicationStyles.ultraViolet }>{ Icons.checkCircle }</FontAwesome>
										<TextHeader
											align="center"
											color={ applicationStyles.ultraViolet }
										>
											&nbsp; { this.props.tags[tag] }
										</TextHeader>
									</View>
								))
							}
						</View>
					) : null
				}
			</View>

		)
	
	}
}

AppRegistry.registerComponent('tatsume', () => PageHero);
