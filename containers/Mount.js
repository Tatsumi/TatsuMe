/**
 * @author Tatsumi Suzuki
 * @description Root component were we map all redux state|dispatch to props.
 */

import React from 'react';

// React Native
import { 
	AppRegistry,
	BackHandler
} from 'react-native';

// Redux
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

// Start component
import { Main } from '../pages/Main';

class Application extends React.Component {

	constructor(props) {
		super(props);
	}

}

function mapStateToProps(state) {

	return {
		leveledUp: state.leveledUp
	};
}


// Make actions accessable from props
function mapDispatchToProps(dispatch) {
	return bindActionCreators({

	}, dispatch);
}

export default Application = connect(mapStateToProps, mapDispatchToProps)(Main);

AppRegistry.registerComponent('tatsume', () => Application);
