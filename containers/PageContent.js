/**
 * @author Tatsumi Suzuki
 * @description Page content component.
 */

// React
import React from 'react';

// React Native
import {
    View,
	Text,
	Linking,
	StyleSheet,
	AppRegistry
} from 'react-native';

// Components
import { TextParagraphWhite } from '../components/TextParagraphWhite';

// Styles
import applicationStyles from '../assets/applicationStyles';


export default class PageContent extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		const styles = StyleSheet.create({
			introWrapper: {
				marginTop: -1,
				padding: 30,
				flexDirection: 'row',
				backgroundColor: `${applicationStyles.overlayColor}`
			},
			longTextWrapper: {
				flexDirection: 'column',
				paddingTop: 30,
				paddingRight: 15,
				paddingBottom: 30,
				paddingLeft: 15,
			},
			link: {
				marginTop: 5,
				marginBottom: 5,
				textDecorationLine: 'underline',
				color: `${applicationStyles.textColor}`
			}
		});

		return (
			<View>
				{
					typeof this.props.introText !== 'undefined' ? (
						<View style={ styles.introWrapper }>
							<TextParagraphWhite
								align="center"
							>
								{ this.props.introText }
							</TextParagraphWhite>
						</View>
					) : null
				}
				<View style={ styles.longTextWrapper }>
					{
						typeof this.props.longText !== 'undefined' ? (
							<TextParagraphWhite>
								{ this.props.longText }
							</TextParagraphWhite>
						) : null
					}
					{
						typeof this.props.linkTo !== 'undefined' && typeof this.props.linkTitle !== 'undefined' ? (
							<Text style={ styles.link } onPress={ () => Linking.openURL(this.props.linkTo) }>
								{ this.props.linkTitle }
							</Text>
						) : null
					}
				</View>
			</View>

		)
	
	}
}

AppRegistry.registerComponent('tatsume', () => PageContent);
