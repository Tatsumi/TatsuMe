/**
 * @author Tatsumi Suzuki
 * @description Leaderboard list component.
 */

// React
import React from 'react';

// React Native
import {
	View,
	Text,
	StyleSheet,
	TouchableHighlight,
	AppRegistry
} from 'react-native';

// Redux
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import { fetchLeaderboard } from '../../actions/userStatistics';

// Styles
import applicationStyles from '../../assets/applicationStyles';

class LeaderBoard extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			leaderboardFetched: {}
		}
	}

	componentDidMount() {
		this.props.fetchLeaderboard();
	}

	static getDerivedStateFromProps(props, state) {

		if(typeof props.leaderboardFetched !== 'undefined' &&
			typeof props.leaderboardFetched.leaderBoard !== 'undefined' &&
			props.leaderboardFetched !== state.leaderboardFetched) {

			return {
				leaderboardFetched: props.leaderboardFetched.leaderBoard
			}
		}

		return null
	}


	_renderLeaderBoard() {

		const styles = StyleSheet.create({
			textWrapper: {
				marginTop: 5,
				flexDirection: 'row',
				justifyContent: 'center',
				alignItems: 'baseline',
			},
			place: {
				width: '10%',
				marginRight: '1%',
				justifyContent: 'flex-start',
			},
			nickWrapper: {
				width: '60%',
				marginRight: '1%',
				justifyContent: 'flex-start',
			},
			strongText: {
				color: `${applicationStyles.textColor}`,
				fontWeight: 'bold'
			},
			text: {
				color: `${applicationStyles.textColor}`,
				fontWeight: 'bold'
			},
			points: {
				width: '25%',
				justifyContent: 'flex-end',
			}
		});

		let leaderBoard = Object.assign([], this.state.leaderboardFetched);

		let leaderBoardReversed = [];
		for (let row in leaderBoard) {
			leaderBoardReversed.push([row, leaderBoard[row]]);
		}

		leaderBoardReversed.reverse();

		return Object.keys(leaderBoardReversed.slice(0, 10)).map((i) => (
			<View key={ 'row-id-' + i } style={ styles.textWrapper }>
				<View style={ styles.place }>
					<Text style={ styles.strongText }>{ parseInt(i) + 1 }.</Text>
				</View>
				<View style={ styles.nickWrapper }>
					<Text style={ styles.text } numberOfLines={1} ellipsizeMode="tail">{ leaderBoardReversed[i][1].nickname}</Text>
				</View>
				<View style={ styles.points }>
					<Text style={ styles.text } numberOfLines={1} ellipsizeMode="tail">{ leaderBoardReversed[i][0]} pt</Text>
				</View>
			</View>
		))
	}

	render() {

		return (
			<View>
				{
					this._renderLeaderBoard()
				}
			</View>
		);
	}
}

function mapStateToProps(state) {

	return {
		leaderboardFetched: state.leaderboardFetched,
		gameSaved: state.gameSaved
	};
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators({
		fetchLeaderboard
	}, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps )(LeaderBoard);

AppRegistry.registerComponent('tatsume', () => LeaderBoard);
