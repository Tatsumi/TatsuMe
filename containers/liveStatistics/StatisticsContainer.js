/**
 * @author Tatsumi Suzuki
 * @description Container wrapping all in game statistics.
 */

// React
import React from 'react';

// React Native
import {
    View,
	Text,
	Dimensions,
	StyleSheet,
	AppRegistry
} from 'react-native';

// Components
import IconUser from '../../components/gameComponents/iconUser';
import IconLifes from '../../components/gameComponents/iconLifes';
import IconBug from '../../components/gameComponents/iconBug';
import IconMergeConflict from '../../components/gameComponents/iconMergeConflict';
import IconFuncion from '../../components/gameComponents/iconFunction';
import IconRelease from '../../components/gameComponents/iconRelease';
import IconDeadline from '../../components/gameComponents/iconDeadline';
import { TextHeader } from '../../components/TextHeader';

// Styles
import applicationStyles from '../../assets/applicationStyles';


export class StatisticsContainer extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		const styles = StyleSheet.create({
			statusWrapper: {
				height: 96,
				padding: 15,
				backgroundColor: `${applicationStyles.overlayColor}`,
				flexDirection: 'row',
				justifyContent: 'space-between',
				alignItems: 'center'
			},
			statusIcons: {
				flexDirection: 'column',
			},
			row1: {
				flex: 1,
				flexDirection: 'row',
			},
			row2: {
				flex: 1,
				flexDirection: 'row',
			},
			column: {
				width: Dimensions.get('window').width >= 375 ? 60 : 40,
				marginTop: Dimensions.get('window').width >= 375 ? 15 : 10,
				marginRight: Dimensions.get('window').width >= 375 ? 15 : 10,
				marginBottom: Dimensions.get('window').width >= 375 ? 15 : 10,
				flexDirection: 'row',
				alignItems: 'center'
			},
			points: {
				flexDirection: 'row',
				alignItems: 'center'
			},
			level: {
				flexDirection: 'row',
				alignItems: 'center'
			},
			levelText: {
				marginLeft: 5,
				color: `${applicationStyles.textColor}`,
				fontFamily: 'Oswald-Medium',
				fontSize: Dimensions.get('window').width >= 375 ? 18 : 16,
			},
			text: {
				height: 16,
				marginLeft: 5,
				color: `${applicationStyles.textColor}`,
			}
		});

		return (
			<View style={ styles.statusWrapper }>
				<View style={ styles.statusIcons }>
					<View style={ styles.row1 }>
						<View style={ styles.column }>
							<IconLifes />
							<Text style={ styles.text }>
								{
									typeof this.props.score.lifes !== 'undefined' ? this.props.score.lifes : 0
								}
							</Text>
						</View>
						<View style={ styles.column }>
							<IconBug />
							<Text style={ styles.text }>
								{
									typeof this.props.score.bugs !== 'undefined' ? this.props.score.bugs : 0
								}
							</Text>
						</View>
						<View style={ styles.column }>
							<IconMergeConflict />
							<Text style={ styles.text }>
								{
									typeof this.props.score.mergeConflicts !== 'undefined' ? this.props.score.mergeConflicts : 0
								}
							</Text>
						</View>
					</View>
					<View style={ styles.row2 }>
						<View style={ styles.column }>
							<IconFuncion />
							<Text style={ styles.text }>
								{
									typeof this.props.score.functions !== 'undefined' ? this.props.score.functions : null
								}
							</Text>
						</View>
						<View style={ styles.column }>
							<IconRelease />
							<Text style={ styles.text }>
								{
									typeof this.props.score.releases !== 'undefined' ? this.props.score.releases : null
								}
							</Text>
						</View>
						<View style={ styles.column }>
							<IconDeadline />
							<Text style={ styles.text }>
								{
									typeof this.props.score.deadlines !== 'undefined' ? this.props.score.deadlines : null
								}
							</Text>
						</View>
					</View>
				</View>
				<View style={ styles.points }>
					<Text style={ styles.levelText }>
						{
							typeof this.props.totalScore !== 'undefined' ? this.props.totalScore + ' ' : null
						}
					</Text>
					<TextHeader>PT</TextHeader>
				</View>
				<View style={ styles.level }>
					<TextHeader>LEVEL</TextHeader>
					<Text style={ styles.levelText }>
						{
							typeof this.props.level !== 'undefined' ? this.props.level : null
						}
					</Text>
				</View>
			</View>

		)
	
	}
}

AppRegistry.registerComponent('tatsume', () => StatisticsContainer);
