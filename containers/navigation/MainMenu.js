/**
 * @author Tatsumi Suzuki
 * @description Mainmenu drawer.
 */

// React
import React from 'react';

// React Native
import {
	AppRegistry,
	View,
	TouchableHighlight,
	StyleSheet
} from 'react-native';

// Redux
import { connect } from 'react-redux';

// Components
import { Hamburger } from './Hamburger';
import { TextHeader } from '../../components/TextHeader';

import applicationStyles from '../../assets/applicationStyles';

const styles = StyleSheet.create({
	navBar: {
		width: '100%',
		height: 'auto',
		backgroundColor: `${applicationStyles.navBarBlue}`,
		justifyContent: 'flex-end',
		flexDirection: 'row'
	},
	main: {
		width: '100%',
		height: '100%',
		flexDirection: 'column',
		alignContent: 'flex-start',
		backgroundColor: `${applicationStyles.overlayColor}`
	},
	navWrapper: {
		marginTop: 30,
		marginRight: 15,
		marginLeft: 15,
	},
	listItemWrapper: {
		marginRight: 5,
		marginLeft: 5,
	}
});

class MainMenu extends React.Component {
	constructor(props) {
		super(props);

		this.state = {
			listItems: [
				{
					name: 'START',
					screen: 'Main',
					selected: true,
					unlocked: true,
				},
				{
					name: 'ABOUT THIS APP',
					screen: 'AboutApp',
					selected: false,
					unlocked: true,
				},
				{
					name: 'ABOUT ME',
					screen: 'AboutMe',
					selected: false,
					unlocked: false,
				},
				{
					name: 'WORK',
					screen: 'Work',
					selected: false,
					unlocked: false,
				},
				{
					name: 'CV/CONTACT',
					screen: 'CV',
					selected: false,
					unlocked: false,
				}
			]
		};
	}

	/**
	 * TODO: Refactor this.
	 * @param {object} nextProps
	 * @constructor
	 */
	UNSAFE_componentWillReceiveProps(nextProps) {
		const userLevel = typeof nextProps.leveledUp !== 'undefined' ? nextProps.leveledUp : {};

		if(typeof userLevel.userLevel !== 'undefined') {
			let updateListItems = this.state.listItems;
			updateListItems[userLevel.userLevel].unlocked = true;

			this.setState({
				listItems: updateListItems
			});

		}
	}

	_updatePageState(screen) {

		if(screen === 'Work') {

			this.props.navigation.navigate(screen, {
				showGrid: true
			});
		} else {
			this.props.navigation.navigate(screen);
		}
	}

	render() {

		return (
			<View style={styles.main}>
				<View style={styles.navBar}>
					<Hamburger open="true" { ...this.props } />
				</View>
				<View style={styles.navWrapper}>
					{
						Object.keys(this.state.listItems).map((listItem) => (
							<View key={'list-item-' + listItem}>
								{
									this.state.listItems[listItem].unlocked ? (
										<TouchableHighlight
											underlayColor="transparent"
											onPress={() => this._updatePageState(this.state.listItems[listItem].screen)}
										>
											<View style={styles.listItemWrapper}>
												<TextHeader
													align="right"
													margin={10}
												>
													{ this.state.listItems[listItem].name }
												</TextHeader>
											</View>
										</TouchableHighlight>
									): (
										<View style={styles.listItemWrapper}>
											<TextHeader
												align="right"
												margin={10}
												decoration="line-through"
											>
												{ this.state.listItems[listItem].name }
											</TextHeader>
										</View>
									)
								}
							</View>
						))
					}
				</View>
			</View>

		)

	}
}

function mapStateToProps(state) {

	return {
		leveledUp: state.leveledUp
	};
}

export default connect(mapStateToProps, null)(MainMenu);

AppRegistry.registerComponent('tatsume', () => MainMenu);
