/**
 * @author Tatsumi Suzuki
 * @description Navigation button and notifications.
 */

// React
import React from 'react';

// React Native
import {
    AppRegistry,
    View,
	StyleSheet,
	TouchableOpacity,
	Platform
} from 'react-native';

import PropTypes from 'prop-types';

// Components
import { Hamburger } from './Hamburger';
import Notification from '../../components/gameComponents/Notification';

// Other
import applicationStyles from '../../assets/applicationStyles';
import FontAwesome, {Icons} from 'react-native-fontawesome';

export default class TopNav extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			unMount: false,
			notification: null,
			backButton: null
		};
	}

	componentDidMount() {

		if(typeof this.props.notification !== 'undefined') {
			this.setState({
				notification: this.props.notification
			});
		}

		const routeName = typeof this.props.navigation.state.routeName !== 'undefined'? this.props.navigation.state.routeName : null;

		if(routeName !== null) {

			switch(routeName) {
				case 'AboutMe':
				case 'AboutApp':
				case 'CV':
					this.setState({
						backButton: 'Mount'
					});
					break;

				default:
					this.setState({
						backButton: null
					});
					break;
			}
		}
	}

	static getDerivedStateFromProps(props, state) {

		if(
			typeof props.clearState !== 'undefined' &&
			props.clearState === true
		) {
			return {
				backButton: 'Work',
				notification: props.notification,
				unMount: false
			}

		} else {
			return {
				notification: props.notification,
				unMount: false
			}
		}
	}

	_unMountNotification(event) {

		if(typeof event.buttonPressed !== 'undefined' && event.buttonPressed === true) {

			if(typeof this.props.shown !== 'undefined') {
				this.props.shown({
					notified: this.state.notification
				});
			}

			setTimeout(() => {
				this.setState({
					unMount: true
				});
			}, 3000);
		}
	}

	_navigate(to) {

		if(to === 'Work') {
			this.props.navigation.navigate(to, {
				showGrid: true
			});

		} else {
			this.props.navigation.navigate(to);
		}
	}

	render() {

		const styles = StyleSheet.create({
			navBar: {
				width: '100%',
				height: 60,
				backgroundColor: `${applicationStyles.navBarBlue}`,
				justifyContent: 'space-around',
				flexDirection: 'row',
				alignItems: 'center',
				paddingRight: 15,
				paddingLeft: 15
			},
			notifications: {
				paddingTop: 6,
				paddingRight: 15,
				paddingBottom: 6,
				paddingLeft: 15,
				width: '80%'
			},
			navigationWrapper: {
				width: 22,
				height: 22
			},
			icon: {
				color: 'white',
				fontSize: 22
			}
		});

		return (

			<View style={styles.navBar}>
				{
					this.state.backButton && Platform.OS === 'ios' ? (
						<TouchableOpacity
							style={ styles.navigationWrapper }
							activeOpacity={0.85}
							onPress={ () =>  this._navigate(this.state.backButton)}
						>
							<FontAwesome style={styles.icon}>
								{ Icons.arrowLeft }
							</FontAwesome>
						</TouchableOpacity>
					) : null
				}
				<View style={styles.notifications}>
					{
						typeof this.state.notification !== 'undefined' && ! this.state.unMount ? (
							<Notification onPressEvent={(event) => this._unMountNotification(event)}>
								{ this.state.notification }
							</Notification>
						) : null
					}
				</View>
				<Hamburger open="false" { ...this.props } />
			</View>
		)
	}
}

TopNav.propTypes = {
	shown: PropTypes.func
};

AppRegistry.registerComponent('tatsume', () => TopNav);
