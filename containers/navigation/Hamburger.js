/**
 * @author Tatsumi Suzuki
 * @description Hamburger menu
 */

// React
import React from 'react';

// React Native
import {
    View,
    TouchableHighlight,
	StyleSheet,
	AppRegistry
} from 'react-native';

import applicationStyles from '../../assets/applicationStyles';

export class Hamburger extends React.Component {

	constructor(props) {
		super(props);
	}

	render() {

		const styles = StyleSheet.create({
			inMenuNav: {
				width: '100%',
				height: 'auto',
				backgroundColor: `${applicationStyles.navBarBlue}`,
				justifyContent: 'flex-end',
				flexDirection: 'row'
			},
			hamburger: {
				width: 40,
				height: 30,
				borderTopWidth: 2,
				borderBottomWidth: 2,
				borderColor: 'white',
				alignItems: 'center',
				justifyContent: 'center'
			},
			hamburgerMiddle: {
				width: 40,
				margin: 0,
				borderBottomWidth: 2,
				borderColor: `${applicationStyles.textColor}`,
			},
			hamburgerCloseWrapper: {
				width: 40,
				height: 30,
				margin: 15,
				alignItems: 'center',
				justifyContent: 'center',
				position: 'relative'
			},
			hamburgerClose1: {
				width: 40,
				borderBottomWidth: 2,
				marginTop: 2,
				borderColor: `${applicationStyles.textColor}`,
				transform: [{rotate: '-45deg'}]
			},
			hamburgerClose2: {
				width: 40,
				borderBottomWidth: 2,
				marginTop: -2,
				borderColor: `${applicationStyles.textColor}`,
				transform: [{rotate: '45deg'}]
			}
		});

		return (
			<View>
				{
					this.props.open === 'true' ? (
						<View style={styles.inMenuNav}>
							<TouchableHighlight
								underlayColor="transparent"
								onPress={() => this.props.navigation.navigate('DrawerClose')}
							>
								<View style={styles.hamburgerCloseWrapper}>
									<View style={styles.hamburgerClose1} />
									<View style={styles.hamburgerClose2} />
								</View>
							</TouchableHighlight>
						</View>
					) : (
						<TouchableHighlight
							onPress={() => this.props.navigation.navigate('DrawerOpen')}
							underlayColor="transparent"
						>
							<View style={styles.hamburger}>
								<View style={styles.hamburgerMiddle}/>
							</View>
						</TouchableHighlight>
					)
				}
			</View>

		)
	
	}
}

AppRegistry.registerComponent('tatsume', () => Hamburger);
