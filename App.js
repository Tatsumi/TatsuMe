/**
 * @author Tatsumi Suzuki
 * @description Main view mounted
 */
 
// React
import React from 'react';

// React Native
import {
	Dimensions,
	SafeAreaView,
	StyleSheet,
	View,
	StatusBar,
	Platform
} from 'react-native';

// React Redux
import { Provider } from 'react-redux';
import store from './store';

// React Navigation
import { DrawerNavigator } from 'react-navigation';

// Components
import MainMenu from './containers/navigation/MainMenu';

// Routes
import StackNavigatorRoutes from './routes/StackNavigatorRoutes';

// DrawerNavigaton
const MainMenuContainer = DrawerNavigator(StackNavigatorRoutes,
    {
        drawerWidth: Dimensions.get('window').width,
        drawerPosition: 'right',
        contentComponent: props => (<MainMenu navigation={props.navigation} drawerProps={{...props}} />)
    }
);

// Helpers

import applicationStyles from './assets/applicationStyles';

const styles = StyleSheet.create({
	appWrapper: {
		flex: 1,
		backgroundColor: `${applicationStyles.navBarBlue}`
	},
	appInnerWrapper: {
		flex: 1
	}
});

export default class App extends React.Component {


	render() {

		const paddingIosTop = Platform.OS === 'ios' ? 5 : 0;

		return (
			<Provider store={store}>
				<View style={[styles.appWrapper, { paddingTop: paddingIosTop }]}>
					<StatusBar barStyle="light-content"/>
					<SafeAreaView style={styles.appInnerWrapper}>
						<MainMenuContainer />
					</SafeAreaView>
				</View>
			</Provider>
		);
	}

}

