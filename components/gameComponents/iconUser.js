/**
 * @author Tatsumi Suzuki
 * @description User icon.
 */

// React
import React from 'react';

// React Native
import {
	Image
} from 'react-native';

import userIcon from '../../assets/img/me.png';

export default class IconUser extends React.Component {

	render() {
		return (
			<Image
				style={[typeof this.props.style !== 'undefined' ? this.props.style : { width: 27, height: 56 }]}
				source={userIcon}
			/>
		);
	}
}

