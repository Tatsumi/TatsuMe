/**
 * @author Tatsumi Suzuki
 * @description Bug icon.
 */

// React
import React from 'react';

// React Native
import {
	Image
} from 'react-native';

import bugIcon from '../../assets/img/bug.png';

export default class IconBug extends React.Component {

	render() {
		return (
			<Image
				style={{ width: 26, height: 26 }}
				source={ bugIcon }
			/>
		);
	}
}

