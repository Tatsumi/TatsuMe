/**
 * @author Tatsumi Suzuki
 * @description Function icon.
 */

// React
import React from 'react';

// React Native
import {
	Image
} from 'react-native';

import functionIcon from '../../assets/img/functions.png';

export default class IconFunction extends React.Component {

	render() {
		return (
			<Image
				style={{ width: 26, height: 26 }}
				source={ functionIcon }
			/>
		);
	}
}

