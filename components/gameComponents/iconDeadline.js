/**
 * @author Tatsumi Suzuki
 * @description Deadline icon.
 */

// React
import React from 'react';

// React Native
import {
	Image
} from 'react-native';

import deadlineIcon from '../../assets/img/deadline.png';

export default class IconDeadline extends React.Component {

	render() {
		return (
			<Image
				style={{ width: 26, height: 26 }}
				source={ deadlineIcon }
			/>
		);
	}
}

