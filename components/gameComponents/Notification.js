/**
 * @author Tatsumi Suzuki
 * @description Dumb component for headers.
 */

// React
import React from 'react';

// React Native
import {
	View,
	Text,
	StyleSheet,
	TouchableHighlight,
	AppRegistry
} from 'react-native';

import PropTypes from 'prop-types';

import FontAwesome, { Icons } from 'react-native-fontawesome';

// Styles
import applicationStyles from '../../assets/applicationStyles';

export default class Notification extends React.Component {

	constructor(props) {
		super(props);

		this.state = {}
	}

	componentDidMount() {

		setTimeout(() => {

			this.props.onPressEvent({
				buttonPressed: true
			});

		}, 2000);
	}

	render() {

		const styles = StyleSheet.create({
			notificationOuterWrapper: {
				flexDirection: 'row',
				justifyContent: 'flex-end',
			},
			notificationWrapper: {
				paddingTop: 6,
				paddingRight: 10,
				paddingBottom: 6,
				paddingLeft: 10,
				flexDirection: 'row',
				justifyContent: 'space-between',
				alignItems: 'center',
				borderRadius: 3,
				backgroundColor: `${applicationStyles.textColor}`,
			},
			notificationText: {
				marginLeft: 5,
				fontFamily: 'Oswald-Medium',
				fontSize: 18,
				color: `${applicationStyles.ultraViolet}`,
			},
			triangle: {
				width: 0,
				height: 0,
				marginTop: 12,
				marginLeft: 0,
				borderStyle: 'solid',
				borderLeftWidth: 8,
				borderRightWidth: 8,
				borderBottomWidth: 16,
				borderLeftColor: 'transparent',
				borderRightColor: 'transparent',
				borderBottomColor: `${applicationStyles.textColor}`,
				backgroundColor: 'transparent',
				transform: [
					{rotate: '90deg'}
				]
			}
		});

		return (
			<TouchableHighlight
				underlayColor="transparent"
				onPress={() => this.props.onPressEvent({
					buttonPressed: true
				})}
			>
				<View style={ styles.notificationOuterWrapper }>
					<View style={ styles.notificationWrapper }>
						<FontAwesome color={ applicationStyles.ultraViolet } style={{ marginTop: 3 }}>{ Icons.timesCircle }</FontAwesome>
						<View>
							<Text style={ styles.notificationText }>{ this.props.children }</Text>
						</View>
					</View>
					<View style={ styles.triangle }></View>
				</View>
			</TouchableHighlight>
		);
	}
}

Notification.propTypes = {
	onPressEvent: PropTypes.func
};

AppRegistry.registerComponent('tatsume', () => Notification);
