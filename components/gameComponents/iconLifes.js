/**
 * @author Tatsumi Suzuki
 * @description Life icon.
 */

// React
import React from 'react';

// React Native
import {
	Image
} from 'react-native';

import lifeIcon from '../../assets/img/lifes.png';

export default class IconLifes extends React.Component {

	render() {
		return (
			<Image
				style={{ width: 26, height: 26 }}
				source={ lifeIcon }
			/>
		);
	}
}

