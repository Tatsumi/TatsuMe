/**
 * @author Tatsumi Suzuki
 * @description Merge conflict icon.
 */

// React
import React from 'react';

// React Native
import {
	Image
} from 'react-native';

import mergeConflictIcon from '../../assets/img/merge-conflict.png';

export default class IconMergeConflict extends React.Component {

	render() {
		return (
			<Image
				style={{ width: 26, height: 26 }}
				source={ mergeConflictIcon }
			/>
		);
	}
}

