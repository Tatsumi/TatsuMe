/**
 * @author Tatsumi Suzuki
 * @description Release icon.
 */

// React
import React from 'react';

// React Native
import {
	Image
} from 'react-native';

import releaseIcon from '../../assets/img/release.png';


export default class IconRelease extends React.Component {

	render() {
		return (
			<Image
				style={{ width: 26, height: 26 }}
				source={releaseIcon}
			/>
		);
	}
}

