/**
 * @author Tatsumi Suzuki
 * @description Component that changes the position of the icons.
 */

// React
import React from 'react';

// React Native
import {
	View,
	StyleSheet
} from 'react-native';

import PropTypes from 'prop-types';

export default class AnimatedIcon extends React.Component {

	constructor(props) {
		super(props);

		this.state = {
			topPosition: 0,
		}
	}

	componentDidMount() {

		let interval = setInterval(() => {

			let currentPosition = this.state.topPosition;
			if(parseInt(currentPosition) < 100) {

				currentPosition = parseInt(currentPosition) + 10;

				this.setState({
					topPosition: currentPosition
				});

			} else {

				currentPosition = 0;

				this.props.animationDone({
					animationDone: true
				});

				clearInterval(interval);

				// Update state only if the view is mounted.
				if(this.refs.isMounted) {
					this.setState({
						topPosition: currentPosition
					});
				}


			}

		}, 150)
	}

	render() {

		const styles = StyleSheet.create({
			icon: {
				position: 'absolute',
				left: 0
			}
		});

		return (
			<View ref="isMounted" style={
				[
					{ top: this.state.topPosition + '%' },
					styles.icon
				]
			}>
				{ this.props.children }
			</View>
		);
	}
}

AnimatedIcon.propTypes = {
	animationDone: PropTypes.func
};
