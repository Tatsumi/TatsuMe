/**
 * @author Tatsumi Suzuki
 * @description Dumb component for White paragraphs.
 */

// React
import React from 'react';

// React Native
import {
	Text,
	StyleSheet,
	AppRegistry
} from 'react-native';


export class TextParagraphWhite extends React.Component {

	render() {

		const styles = StyleSheet.create({
			font: {
				fontSize: 16,
				textAlign: typeof this.props.align !== 'undefined' ? this.props.align : 'left',
				color: 'white',
				marginTop: typeof this.props.margin !== 'undefined' ? this.props.margin : 0,
				marginBottom: typeof this.props.margin !== 'undefined' ? this.props.margin : 0,
				lineHeight: 23,
				fontWeight: "normal"
			}
		});

		return (
			<Text style={styles.font}>{this.props.children}</Text>
		);
	}
}


AppRegistry.registerComponent('tatsume', () => TextParagraphWhite);
