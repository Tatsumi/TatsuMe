/**
 * @author Tatsumi Suzuki
 * @description Dumb component for buttons.
 */

// React
import React from 'react';

// React Native
import {
	Text,
	TouchableOpacity,
	StyleSheet,
	AppRegistry,
	View
} from 'react-native';

import PropTypes from 'prop-types';
import applicationStyles from '../assets/applicationStyles';

export class AppButton extends React.Component {

	constructor(props) {
		super(props);

		this._onPress = this._onPress.bind(this);
	}

	_onPress() {
		this.props.onPressEvent({
			buttonPressed: true
		});
	}

	render() {
		const styles = StyleSheet.create({
			container: {
				justifyContent: 'center',
				alignItems: 'center'
			},
			font: {
				fontFamily: 'Oswald-Medium',
				fontSize: 16,
				color: typeof this.props.fontColor !== 'undefined' ? this.props.fontColor : applicationStyles.textColor,
				alignItems: 'center'
			},
			button: {
				minWidth: 143,
				maxWidth: typeof this.props.maxWidth !== 'undefined' ? this.props.maxWidth : 143,
				margin: 15,
				alignItems: 'center',
				backgroundColor: typeof this.props.backgroundColor !== 'undefined' ? this.props.backgroundColor : applicationStyles.ultraViolet,
				paddingTop: 8,
				paddingRight: 30,
				paddingBottom: 8,
				paddingLeft: 30,
				borderRadius: 3
			}
		});
		return (
			<View style={styles.container}>
				<TouchableOpacity
					activeOpacity={0.85}
					style={styles.button}
					onPress={this._onPress}
				>
					<Text style={styles.font}>{this.props.children}</Text>
				</TouchableOpacity>
			</View>
		);
	}
}

AppButton.propTypes = {
	onPressEvent: PropTypes.func
};

AppRegistry.registerComponent('tatsume', () => AppButton);
