/**
 * @author Tatsumi Suzuki
 * @description Dumb component for headers.
 */

// React
import React from 'react';

// React Native
import {
	Text,
	StyleSheet,
	AppRegistry,
	Dimensions
} from 'react-native';


export class TextHeader extends React.Component {

	render() {

		const styles = StyleSheet.create({
			font: {
				fontFamily: 'Oswald-Medium',
				fontSize: Dimensions.get('window').width >= 375 ? 18 : 16,
				textAlign: typeof this.props.align !== 'undefined' ? this.props.align : 'left',
				marginTop: typeof this.props.margin !== 'undefined' ? this.props.margin : 0,
				marginBottom: typeof this.props.margin !== 'undefined' ? this.props.margin : 0,
				color: typeof this.props.color !== 'undefined' ? this.props.color : 'white',
				textDecorationLine: typeof this.props.decoration !== 'undefined' ? this.props.decoration : 'none',
			}
		});
		return (
			<Text style={styles.font} numberOfLines={1} ellipsizeMode="tail">{this.props.children}</Text>
		);
	}
}


AppRegistry.registerComponent('tatsume', () => TextHeader);
