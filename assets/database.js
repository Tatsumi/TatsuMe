import * as firebase from 'firebase';

const config = {
	apiKey: "AIzaSyDmyD7jZFc5YExwsstN4AfaNR6YTE2cnXQ",
	authDomain: "tatsumi-me.firebaseapp.com",
	databaseURL: "https://tatsumi-me.firebaseio.com",
	projectId: "tatsumi-me",
	storageBucket: "tatsumi-me.appspot.com",
	messagingSenderId: "861079137100"
};

firebase.initializeApp(config);

export default firebase;
