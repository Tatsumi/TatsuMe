const pageContentStrings = {
	aboutPage: {
		title: 'ABOUT THIS APP',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/tatsumi.jpg?alt=media&token=f64d05ef-fe0a-4823-8a7c-66d19c9e27b2',
		introText: 'My goal with this application is to introduce myself in a fun way.\n' +
		'I also wan\'t to have something to show to my future colleagues.',
		longText: 'This application is built with React Native and Redux. I have used Firebase Storage for images and Firebase realtime database for the score board.\n' +
		'The source code  is located at Gitlab. Follow the link below.',
		linkTo: 'https://gitlab.com/Tatsumi/TatsuMe',
		linkTitle: 'gitlab.com'
	},
	aboutMe: {
		title: 'THIS IS ME',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/tatsumi.jpg?alt=media&token=f64d05ef-fe0a-4823-8a7c-66d19c9e27b2',
		tags: [
			'DEVELOPER',
			'ENTREPRENEUR',
			'DJ/ARTIST',
			'HOMEBREWER'
		],
		introText: 'I\'m 38 years old, born and raised in Stockholm.\n' +
		'When I\'m not busy writing applications or making application design, you might probably find me behind the dj-decks, brewing beer or out running or enjoying life with my boat.',
		longText: 'I started with front end development 10 years ago. I have since then worked at Stardoll and later at Radioplay.\n' +
		'In 2015 I started my own firm together with my childhood friend. Fast Forward Media (FFMedia AB) was a digital agency. We made websites, mobile applications, digital strategies and photography. During that time I was in charge of the entire production from the design work to programming. I was responsible for total of 10 interns from Medieinstitutet, Nackademin, Södertörns Högskola and KYH.\n' +
		'\n' +
		'We started as two colleagues and ended up as three full time employees and another two external resources.\n' +
		'\n' +
		'Start up life has given me responsibility in a whole new level. I have become more flexible in my position and I have become more mature in programming, design, management and having employees. We as many startup companies didn\'t get the revenue we was hoping for so we decided in May 2018 to end that journey and put the company down.\n' +
		'\n' +
		'I’m now searching for new great colleagues and a new interesting job. I would like to have a role where I can practice programming combined with design or leadership.'
	},
	radioplay: {
		title: 'RADIOPLAY',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/ffm_radioplay2.jpg?alt=media&token=dc8489bc-e4c0-4595-b450-ee6927ea8631',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/rp-1-1.jpg?alt=media&token=558c8bcb-9a2c-49eb-be00-6535792bbf6d',
		tags: [
			'REACT',
			'FLUX'
		],
		introText: 'Radioplay with NRJ, Mix Megapol, Rock klassiker, The Voice to mention a few radio stations and all their podcasts is owned by Bauer Media and is one of the largest radio companies in Europe.',
		longText: 'I have worked for Radioplay as a frontend developer for 2.5 years and as an consultant for Fast Forward Media.\n' +
		'The site is developed with React and I worked across the site with the player, podcasts, ads and responsiveness.',
		linkTo: 'https://radioplay.se',
		linkTitle: 'Radioplay.se'
	},
	wupza: {
		title: 'WUPZA',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/wupza_hero3.jpg?alt=media&token=c0c73c7f-2e59-4e03-8cbf-9acc8e2727a6',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/wupza_1-1.jpg?alt=media&token=6ad51a48-6282-4a40-8515-616b42123fef',
		tags: [
			'REACT',
			'REDUX',
			'FIREBASE'
		],
		introText: 'A complex React and React Native application.\n' +
		'I was in lead of the architecture and the web design of the application.',
		longText: 'With this company ISO focused companies with their mobile application users can fill forms when something in their work goes wrong, whistle blow or submit suggestions. Nearest chief or the board members can then take action on those reports. All companies can skin their application with their logo and colors to make the application look more suited for the firm. All forms are also built with a custom form builder for each company. Users daily mood is also monitored in the mobile application.\n' +
		'Wupza is available for both iOS and Android.',
		linkTo: 'https://wupza.com',
		linkTitle: 'Wupza.com'
	},
	stardoll: {
		title: 'STARDOLL',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/stardoll.jpg?alt=media&token=bdeaa3b3-4345-4aaf-b64e-1e70eb09170e',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/stardoll-1-1.jpg?alt=media&token=dc2c4595-d803-4597-bcf1-2b26f24973d1',
		tags: [
			'PHP',
			'MYSQL',
			'JQUERY'
		],
		introText: 'Design heavy community that had 250 million user accounts and 1.5 million daily page views.',
		longText: 'Stardoll was my first job in this line of business. I worked there during 2010 - 2013  as a front end developer. \n' +
		'This was before the web got responsive and when developers used css sprites and rounded corner hacks and had to support IE6. My first assignment was to build the community for the arabic speaking users. I also worked with payments and for the campaign team making user integrated campaigns for Nintendo, Hasbro, Mattel, Disney, Lego and EA to mention a few.',
		linkTo: 'https://stardoll.com',
		linkTitle: 'Stardoll.com'
	},
	mother: {
		title: 'MOTHER - GRUPP F12',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/mother.jpg?alt=media&token=c36460fe-4938-4858-8492-69683b9d099b',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/mother-1-1.jpg?alt=media&token=e5d4c064-6a0b-4fc7-877a-20cacbee5e05',
		tags: [
			'PHP',
			'MYSQL',
			'JQUERY'
		],
		introText: 'Nutrition calculator for the chefs at a restaurant called Mother that belongs to the F12 group in Stockholm.',
		longText: 'During my first year with Fast Forward Media we got a job to create a smart calculator for a new restaurant called Mother. Their mission was to cook healthy food and use tablets to show their customers all the nutrition values. I used Codeigniter 3 as a PHP framework and jQuery for the ajax calls to a nutrition api that returned values for each ingredients.',
	},
	ffm: {
		title: 'FAST FORWARD MEDIA',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/ffm_hero1.jpg?alt=media&token=8d2cf4b5-8da4-415d-a5cc-b71bbb215f44',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/ffm_hero1-1.jpg?alt=media&token=e8cdd81b-fbd1-4e65-8264-362b3c016c2a',
		tags: [
			'CTO',
			'MEMBER OF THE BOARD'
		],
		introText: 'Fast Forward Media (FFMedia AB) was a firm that I had with my childhood friend for three years 2015 - 2018.',
		longText: 'Fast Forward Media was a small digital agency with three full time employees and two part time employees. I was in charge of the web design and all development related matters. I was a technical mentor to a total of  10 students from Medieinstitutet, Nackademin, KYH and Södertörns Högskola. During those years, we made websites with PHP, React and also released an iOS and Android application with React Native.',
	},
	viasat: {
		title: 'VIASAT',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/viasat.jpg?alt=media&token=ea0863bb-af87-4abb-a1a6-fd385e1be4a4',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/viasat1-1.jpg?alt=media&token=d31b3b23-4c3b-4eca-b555-2763374b1b26',
		tags: [
			'HTML'
		],
		introText: 'Re-designed order confirmations and contracts between the customer and Viasat.',
		longText: 'I made html templates for automatic generated pdf order confirmations or contracts between Viasat and the customer.',
	},
	taylors: {
		title: 'TAYLORS & JONES',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/taylors-startsida.jpg?alt=media&token=ec1f9466-fc8d-4c31-860d-f700be31745c',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/taylors-insta.jpg?alt=media&token=ecd819b8-2e56-43bc-a9d4-b8dfd4ac7a6f',
		tags: [
			'WORDPRESS'
		],
		introText: 'Design heavy website with lazy loading and image optimisation.',
		longText: 'I made a new website for Taylors & Jones from a performance heavy web design. Lazy loading images and other optimisation is a must!',
		linkTo: 'https://taylorsandjones.com',
		linkTitle: 'Taylorsandjones.com'
	},
	sofo: {
		title: 'SOFO',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/sofo-hero-2560x1280_2.jpg?alt=media&token=8affb873-0ffe-43ff-924e-33e82480161d',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/sofo-hero1-1.jpg?alt=media&token=cf239039-fbd9-407e-9d3d-ebc73bec5f3f',
		tags: [
			'WORDPRESS',
			'COMMUNITY'
		],
		introText: 'Website for the companies and visitors around the SoFo (South of Folkungagatan) neighbourhood.',
		longText: 'I made a small community for the companies (Företagarföreningen SoFo) of the SoFo area.\n' +
		'All boutiques is sorted by category and with a dot in a map. The boutiques all have their own page with information, opening hours and feeds.\n' +
		'I made the web design and all coding behind with Wordpress.',
		linkTo: 'http://sofo-stockholm.se',
		linkTitle: 'Sofo-stockholm.se'
	},
	mypal: {
		title: 'MYSELF-ESTEEM PAL',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/mypal-hero-2560x1280.jpg?alt=media&token=20f20fe5-a264-4178-b456-e83b73c3bada',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/mypal-insta.jpg?alt=media&token=0d65e67f-6314-4886-bcac-ecb44c1e05f4',
		tags: [
			'REACT',
			'REDUX',
			'FIREBASE',
			'STRIPE'
		],
		introText: 'MySelf-esteem Pal is an online diary tool that will help you practice positive thinking and learn to appreciate yourself more.',
		longText: 'The product is an responsive mobile first web application made with React, Redux, Firebase and payments with Stripe.\n' +
		'I did all the programming, web design and the illustrations of the "pals".',
		linkTo: 'https://myselfesteempal.com',
		linkTitle: 'Myselfesteempal.com'
	},
	enjis: {
		title: 'ENJIS',
		image: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/12764433_956974507724328_6880423847274359425_o.jpg?alt=media&token=e17aa63c-c297-4d1f-9943-0dd6b7f914d7',
		imageSquare: 'https://firebasestorage.googleapis.com/v0/b/tatsumi-me.appspot.com/o/enjis1-1.jpg?alt=media&token=a10f6cbe-b786-41d2-8c9d-cb414212b90b',
		tags: [
			'WEBSITE',
			'MOBILE APPLICATION'
		],
		introText: 'I made a website and a mobile application for daily work reports.',
		longText: 'Enjis is a blasting company. I have made an mobile web application for them to use on daily basis for blasting reports and daily work.\n' +
		'The work get\'s signed by the chief of the working site and the result can be used when the invoice will be sent from the firm.\n' +
		'I have also designed and worked with their website as well.',
		linkTo: 'https://enjisbergsprangning.se',
		linkTitle: 'Enjisbergsprangning.se'
	},
};

export default pageContentStrings;
