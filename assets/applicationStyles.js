const defaultStyles = {
	backgroundColor: 'rgb(49, 67, 89)',
	overlayColor: 'rgb(33, 48, 62)',
	navBarBlue: 'rgb(22, 35, 45)',
	textColor: 'rgb(255, 255, 255)',
	ultraViolet: 'rgb(85, 67, 125)'
};

export default defaultStyles;
