// Redux
import { 
	applyMiddleware, 
	createStore
} from 'redux';

// Redux Thunk Middleware
import thunk from 'redux-thunk';

// Redux Promise Middleware
import promise from 'redux-promise-middleware';

import { createLogger } from 'redux-logger';

// Reducers
import rootReducer from './reducers';

// Promise enables handling of async code in redux, thunk enables chained async actions.
const middleware = applyMiddleware(promise(), thunk, createLogger());

export default createStore(rootReducer, middleware);
